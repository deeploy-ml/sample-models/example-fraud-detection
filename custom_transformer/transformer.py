import kserve
import sys
from typing import Dict
import logging
import dill
import numpy as np
from omnixai.data.tabular import Tabular

from parser_transformer import parse_args

logging.basicConfig(level=kserve.constants.KSERVE_LOGLEVEL)

PATH_TO_TRANSFORMER = "transformer/transformer.dill"
FEATURE_COLUMNS = [
    "step",
    "type",
    "amount",
    "oldBalanceOrig",
    "newBalanceOrig",
    "oldBalanceDest",
    "newBalanceDest",
    "orig_diff",
    "dest_diff",
    "surge",
    "freq_dest",
    "merchant",
    "customers_org",
    "customers_dest",
]
CATEGORIAL_COLUMNS = ["type"]


class Transformer(kserve.Model):
    def __init__(self, name: str, predictor_host: str, explainer_host: str):
        super().__init__(name)
        self.predictor_host = predictor_host
        self.explainer_host = explainer_host
        self.transformer_file = f"{PATH_TO_TRANSFORMER}"
        with open(self.transformer_file, "rb") as f:
            logging.info("Loading Transformer")
            self.transformer = dill.load(f)
            logging.info("Loaded Transformer")
        self.ready = True

    def convertToTabular(self, input):
        dataFrame = np.array(input)
        tabular_data = Tabular(
            dataFrame,
            feature_columns=FEATURE_COLUMNS,
            categorical_columns=CATEGORIAL_COLUMNS,
        )
        return tabular_data

    def transformation(self, input):
        # uses omni transformer TabularTransform
        val = self.transformer.transform(self.convertToTabular(input))
        return val.tolist()

    def preprocess(self, inputs: Dict, headers: Dict[str, str] = None) -> Dict:
        return {"instances": self.transformation(inputs["instances"])}

    def postprocess(self, inputs: Dict, headers: Dict[str, str] = None) -> Dict:
        return inputs


if __name__ == "__main__":
    logging.info("Loading Transformer Module")
    args = parse_args(sys.argv)
    transformer = Transformer(
        args.model_name,
        predictor_host=args.predictor_host,
        explainer_host=args.explainer_host,
    )
    logging.info("Loaded Transformer Module")
    kserve.ModelServer().start(models=[transformer])
