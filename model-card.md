### Model Name
Fraud Detection model

### Model description
Gaussian Naive Bayes
### Model date
2023-10-09
### Model type
Supervised Learning - tabular classification

### Languages
Python

### Library Name
Scikit-learn

### Dataset
Synthetic Financial Dataset For Fraud Detection
https://www.kaggle.com/datasets/ealaxi/paysim1/data

### Model performance

|             | Precision   | Recall      | F1-Score    | Support     |
| ----------- | ----------- | ----------- | ----------- | ----------- |
| Not Fraud   | 0.99        | 1.00        | 0.99        | 8937        |
| Fraud       | 1.00        | 0.99        | 0.99        | 9013        |
|             |             |             |             |             |
| Accuracy    |             |             | 0.99        | 17950       |   
| Macro Avg   | 0.99        | 0.99        | 0.99        | 17950       |
| Weighted Avg| 0.99        | 0.99        | 0.99        | 17950       |


Average precision score:  0.9923

F1 score:  0.9947


### Limitations
This model is built with a simple synthetic dataset and is hence not representative of real-life use cases of fraud detection. It should therefore only be used for demonstration purposes. 

