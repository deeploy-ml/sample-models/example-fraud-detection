import os
import pandas as pd
from joblib import dump
import numpy as np
import dill

# Scikit-learn models and accessories
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier

# Metrics Libraries
from sklearn.metrics import classification_report
from sklearn.model_selection import cross_val_score
from sklearn.metrics import average_precision_score, f1_score

# Data processing libraries
from omnixai.data.tabular import Tabular
from omnixai.preprocessing.tabular import TabularTransform
from omnixai.preprocessing.encode import OneHot

# Read the data
data = pd.read_pickle("../data/preprocessed_data.pkl")

# Reindex to put target column furthest right to make train split slicing easier
data = data.reindex(
    columns=[
        "step",
        "type",
        "amount",
        "oldBalanceOrig",
        "newBalanceOrig",
        "oldBalanceDest",
        "newBalanceDest",
        "orig_diff",
        "dest_diff",
        "surge",
        "freq_dest",
        "merchant",
        "customers_org",
        "customers_dest",
        "isFraud",
    ]
)

# Make Tabular object for the data which is compatible with the MACE explainer.
tabular_data = Tabular(data=data, categorical_columns=["type"], target_column="isFraud")

# Instantiate transformer to one hot encode the 'type' column
transformer = TabularTransform(cate_transform=OneHot()).fit(tabular_data)

# Path to save the transformer as a dill file
file_path = os.path.join("../transformer", "transformer.dill")

# Dump the transformer object to 'transformer.dill'
with open(file_path, "wb") as f:
    dill.dump(transformer, f)


# Create training data by first transforming input and splitting into train and test set
x = transformer.transform(tabular_data)
X_train, X_test, y_train, y_test = train_test_split(
    x[:, :-1], x[:, -1], train_size=0.7, random_state=42
)

# Create experiment setup to find best model
logreg_cv = LogisticRegression(solver="liblinear", random_state=123)
dt_cv = DecisionTreeClassifier(random_state=123)
knn_cv = KNeighborsClassifier()
nb_cv = GaussianNB()
rf_cv = RandomForestClassifier(random_state=123)
cv_dict = {
    0: "Logistic Regression",
    1: "Decision Tree",
    2: "KNN",
    3: "SVC",
    4: "Naive Bayes",
    5: "Random Forest",
}
cv_models = [logreg_cv, dt_cv, knn_cv, nb_cv, rf_cv]

# Run experiment
for i, model in enumerate(cv_models):
    print(
        "{} Test Accuracy: {}".format(
            cv_dict[i],
            cross_val_score(model, X_train, y_train, cv=10, scoring="accuracy").mean(),
        )
    )
param_grid_nb = {"var_smoothing": np.logspace(0, -9, num=100)}

# Depending on results from experiment choose an algorithm.
# Here Gaussian Naive Bayes is chosen.
nbModel_grid = GridSearchCV(
    estimator=GaussianNB(), param_grid=param_grid_nb, verbose=1, cv=10, n_jobs=-1
)
nbModel_grid.fit(X_train, y_train)
print("Best model: ", nbModel_grid.best_estimator_)

# Predict with the selected best parameters
y_pred = nbModel_grid.predict(X_test)


# Classification metrics
print(classification_report(y_test, y_pred, target_names=["Not Fraud", "Fraud"]))
print("Average precision score: ", average_precision_score(y_test, y_pred))
print("F1 score: ", f1_score(y_test, y_pred))


# Save the model
model = nbModel_grid.best_estimator_
dump(model, "../model/model.joblib")
