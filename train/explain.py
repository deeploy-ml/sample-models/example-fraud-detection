from joblib import load
import pandas as pd
from omnixai.data.tabular import Tabular
from omnixai.preprocessing.tabular import TabularTransform
from omnixai.preprocessing.encode import OneHot
from omnixai.explainers.tabular import TabularExplainer
import dill

# Load model
clf = load("../model/model.joblib")
# Read data
data = pd.read_pickle("../data/preprocessed_data.pkl")

# Make Tabular object for the data which is compatible with the MACE explainer.
# Define which columns are categorical
tabular_data = Tabular(data=data, categorical_columns=["type"], target_column="isFraud")

# Instantiate transformer to one hot encode the 'type' column
transformer = TabularTransform(cate_transform=OneHot()).fit(tabular_data)

# Specify more informative class names
class_names = ["not Fraud", "Fraud"]

# Create MACE explainer
mace_explainer = TabularExplainer(
    explainers=["mace"],
    mode="classification",
    data=tabular_data,
    model=clf,
    preprocess=lambda z: transformer.transform(z),
    postprocess=None,
    # Optional: can set which features to ignore when generating counterfactuals
    # params={
    #     "mace": {"ignored_features": ["Sex", "Race", "Relationship", "Capital Loss"]}
    # }
)

# Before serialising and deploying in Deeploy we need to set the model and preprocess
# attributes to None. This has to do with the way that the explanations interact with
# the model in the Deeploy platform
mace_explainer.model = None
mace_explainer.preprocess = None

# The explainer needs to be serialised as a list object, and since the explainer depends
# on the Tabular Transform we need to include it in the list object like so:
# [explainer, transformer]
mace_lstobject = [mace_explainer, transformer]

# Serialise explainer as a dill object, see above for structure
with open("../explainer/explainer.dill", "wb") as f:
    dill.dump(mace_lstobject, f)
