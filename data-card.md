# Fraud Detection Data Card
* **Dataset publish date**: 01-01-2017

## Synthetic Financial Datasets For Fraud Detection
The following was retrieved from [Kaggle](https://www.kaggle.com/datasets/ealaxi/paysim1/data).

There is a lack of public available datasets on financial services and specially in the emerging mobile money transactions domain. Financial datasets are important to many researchers and in particular to us performing research in the domain of fraud detection. Part of the problem is the intrinsically private nature of financial transactions, that leads to no publicly available datasets.

They present a synthetic dataset generated using the simulator called PaySim as an approach to such a problem. PaySim uses aggregated data from the private dataset to generate a synthetic dataset that resembles the normal operation of transactions and injects malicious behaviour to later evaluate the performance of fraud detection methods.

## Dataset details
**Dataset Characteristics**: Multivariate  
**Subject Area**: Financial crime  
**Associated Task**: Classification  
**Feature Type**: Categorical, Continuous
**Number of Instances**: 6362620  
**Number of Features**: 11

## Features
| Variable Name | Role | Type | Description | Missing Values |
|---|---|---|---|---|
| step | Feature | Integer | maps a unit of time in the real world. In this case 1 step is 1 hour of time. Total steps 744 (30 days simulation) | no |
| type | Feature | Categorical | CASH-IN, CASH-OUT, DEBIT, PAYMENT and TRANSFER | no |
| amount | Feature | Float | amount of the transaction in local currency | no |
| nameOrig | Feature | Categorical | customer who started the transaction | no |
| oldbalanceOrg | Feature | Float | initial balance before the transaction | no |
| newbalanceOrg | Feature | Float | new balance after the transaction | no |
| nameDest | Feature | Categorical | customer who is the recipient of the transaction | no |
| oldbalanceDest | Feature | Float | initial balance recipient before the transaction. Note that there is not information for customers that start with M (Merchants) | no |
| newbalanceDest | Feature | Float | new balance recipient after the transaction. Note that there is not information for customers that start with M (Merchants) | no |
| isFraud | Target | Binary | This is the transactions made by the fraudulent agents inside the simulation. In this specific dataset the fraudulent behavior of the agents aims to profit by taking control or customers accounts and try to empty the funds by transferring to another account and then cashing out of the system | no |
| isFlaggedFraud | Target | Binary | The business model aims to control massive transfers from one account to another and flags illegal attempts. An illegal attempt in this dataset is an attempt to transfer more than 200.000 in a single transaction | no |

## Samples
The fraud detection model has been created for demoing purposes, therefore this dataset has been sampled down to 28632 samples.
